package my.edu.utem.ftmk.css.model;

import java.util.Date;

/**
 * Represents Schedule data on Schedule table in db
 * @author Syafiq Azlan
 * @version 1.0
 */
public class Schedule 
{
	private int scheduleID;
	private Date start;
	private int duration;
	private int clazzID;
	private int venueID;
	
	/**
	 * @return the scheduleID
	 */
	public int getScheduleID() {
		return scheduleID;
	}
	/**
	 * @param scheduleID the scheduleID to set
	 */
	public void setScheduleID(int scheduleID) {
		this.scheduleID = scheduleID;
	}
	/**
	 * @return the start
	 */
	public Date getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(Date start) {
		this.start = start;
	}
	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	/**
	 * @return the clazzID
	 */
	public int getClazzID() {
		return clazzID;
	}
	/**
	 * @param clazzID the clazzID to set
	 */
	public void setClazzID(int clazzID) {
		this.clazzID = clazzID;
	}
	/**
	 * @return the venueID
	 */
	public int getVenueID() {
		return venueID;
	}
	/**
	 * @param venueID the venueID to set
	 */
	public void setVenueID(int venueID) {
		this.venueID = venueID;
	}

}
