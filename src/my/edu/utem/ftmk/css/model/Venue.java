package my.edu.utem.ftmk.css.model;
/**
 * Represents Venue data on Venue table in db
 * @author Syafiq Azlan
 * @version 1.0
 */
public class Venue 
{
	private int venueID;
	private String venueName;
	/**
	 * @return the venueID
	 */
	public int getVenueID() {
		return venueID;
	}
	/**
	 * @param venueID the venueID to set
	 */
	public void setVenueID(int venueID) {
		this.venueID = venueID;
	}
	/**
	 * @return the venueName
	 */
	public String getVenueName() {
		return venueName;
	}
	/**
	 * @param venueName the venueName to set
	 */
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	

	
	
}
