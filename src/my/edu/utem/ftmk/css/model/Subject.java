package my.edu.utem.ftmk.css.model;
/**
 * Represents subject data on subject table in db
 * @author Syafiq Azlan
 * @version 1.0
 */
public class Subject 
{
	private String subjectID;
	private String name;
	
	/**
	 * @return the subjectID
	 */
	public String getSubjectID() {
		return subjectID;
	}
	/**
	 * @param string the subjectID to set
	 */
	public void setSubjectID(String string) {
		this.subjectID = string;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() 
	{
		return subjectID + " - " + name;
	}
	
	@Override
	public int hashCode()
	{
		String merged = subjectID + "&&" + name;
		return merged.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return hashCode() == obj.hashCode();
	}
}
