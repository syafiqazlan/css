package my.edu.utem.ftmk.css.model;
/**
 * Represents Grouping data on Grouping table in db
 * @author Syafiq Azlan
 * @version 1.0
 */
public class Grouping implements Comparable<Grouping>
{
	private int groupingID;
	private int yearOfStudy;
	private String course;
	private int sectionNo;
	private int groupNo;
	
	/**
	 * @return the groupingID
	 */
	public int getGroupingID() {
		return groupingID;
	}
	/**
	 * @param groupingID the groupingID to set
	 */
	public void setGroupingID(int groupingID) {
		this.groupingID = groupingID;
	}
	/**
	 * @return the yearOfStudy
	 */
	public int getYearOfStudy() {
		return yearOfStudy;
	}
	/**
	 * @param yearOfStudy the yearOfStudy to set
	 */
	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}
	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}
	/**
	 * @param course the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}
	/**
	 * @return the sectionNo
	 */
	public int getSectionNo() {
		return sectionNo;
	}
	/**
	 * @param sectionNo the sectionNo to set
	 */
	public void setSectionNo(int sectionNo) {
		this.sectionNo = sectionNo;
	}
	/**
	 * @return the groupNo
	 */
	public int getGroupNo() {
		return groupNo;
	}
	/**
	 * @param groupNo the groupNo to set
	 */
	public void setGroupNo(int groupNo) {
		this.groupNo = groupNo;
	}
	
	@Override
	public int hashCode()
	{
		return	toString().hashCode();
	}
	
	@Override
	public String toString()
	{
		return yearOfStudy + course + " S" + sectionNo + "/G" + groupNo;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return hashCode() == obj.hashCode();
	}
	@Override
	public int compareTo(Grouping that)
	{
		return toString().compareTo(that.toString());
	}
	
}