package my.edu.utem.ftmk.css.model;
/**
 * Represents Clazz data on Clazz table in db
 * @author Syafiq Azlan
 * @version 1.0
 */
public class Clazz implements Comparable<Clazz>
{
	private int clazzID;
	private int type;
	private String staffID;
	private String subjectID;
	private int groupingID;
	private String staffName, subjectName, groupingName;
	
	/**
	 * @return the staffName
	 */
	public String getStaffName()
	{
		return staffName;
	}
	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName)
	{
		this.staffName = staffName;
	}
	/**
	 * @return the subjectName
	 */
	public String getSubjectName()
	{
		return subjectName;
	}
	/**
	 * @param subjectName the subjectName to set
	 */
	public void setSubjectName(String subjectName)
	{
		this.subjectName = subjectName;
	}
	/**
	 * @return the groupingName
	 */
	public String getGroupingName()
	{
		return groupingName;
	}
	/**
	 * @param groupingName the groupingName to set
	 */
	public void setGroupingName(String groupingName)
	{
		this.groupingName = groupingName;
	}
	/**
	 * @return the clazzID
	 */
	public int getClazzID() {
		return clazzID;
	}
	/**
	 * @param clazzID the clazzID to set
	 */
	public void setClazzID(int clazzID) {
		this.clazzID = clazzID;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the staffID
	 */
	public String getStaffID() {
		return staffID;
	}
	/**
	 * @param staffID the staffID to set
	 */
	public void setStaffID(String staffID) {
		this.staffID = staffID;
	}
	/**
	 * @return the subjectID
	 */
	public String getSubjectID() {
		return subjectID;
	}
	/**
	 * @param subjectID the subjectID to set
	 */
	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}
	/**
	 * @return the groupingID
	 */
	public int getGroupingID() {
		return groupingID;
	}
	/**
	 * @param groupingID the groupingID to set
	 */
	public void setGroupingID(int groupingID) {
		this.groupingID = groupingID;
	}
	
	@Override
	public int compareTo(Clazz clazz)
	{
		int status = 0;
		
		if (type < clazz.type)
		{
			status = -1;
		}
		else if (type > clazz.type)
		{
			status = 1;
		}
		else if (type == clazz.type)
		{
			status = staffName.compareTo(clazz.staffName);

			if (status == 0)
			{
				status = subjectName.compareTo(clazz.subjectName);
				if (status == 0)
					status = groupingName.compareTo(clazz.groupingName);
			}
		}
		
		return status;
	}

	@Override
	public String toString()
	{
		return null;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return hashCode() == obj.hashCode();
	}
	
	@Override
	public int hashCode()
	{
		String merged = clazzID + "::" + type + ";;" + staffID + "--" + subjectID + "&&" + groupingID;
		
		return merged.hashCode();
	}
	

}
