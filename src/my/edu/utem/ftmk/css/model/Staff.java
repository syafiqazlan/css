package my.edu.utem.ftmk.css.model;
/**
 * Represents staff data on staff table in db
 * @author Syafiq Azlan
 * @version 1.0
 */
public class Staff 
{
	private String StaffID;
	private String password;
	private String name;
	private int role;
	
	/**
	 * @return the staffID
	 */
	public String getStaffID() 
	{
		return StaffID;
	}
	
	/**
	 * @param staffID the staffID to set
	 */
	public void setStaffID(String staffID) 
	{
		StaffID = staffID;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() 
	{
		return password;
	}
	
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	/**
	 * @return the name
	 */
	public String getName() 
	{
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) 
	{
		this.name = name;
	}
	
	/**
	 * @return the role
	 */
	public int getRole() 
	{
		return role;
	}
	
	/**
	 * @param role the role to set
	 */
	public void setRole(int role) 
	{
		this.role = role;
	}
	
	@Override
	public int hashCode()
	{
		String merged = StaffID + "&&" + password + "::" + name +"--" + role;
		return merged.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return hashCode() == obj.hashCode();
	}
	
	@Override
	public String toString() 
	{
		return StaffID + " - " +name;
	}
}
