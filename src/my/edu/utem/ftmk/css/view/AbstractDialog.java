package my.edu.utem.ftmk.css.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public abstract class AbstractDialog extends JDialog implements ActionListener
{
	private static final long serialVersionUID = 1L;
	protected GridBagLayout layout = new GridBagLayout();
	protected JPanel pnlCenter = new JPanel(layout);
	protected JPanel pnlSouth = new JPanel(new FlowLayout(FlowLayout.RIGHT));
	protected MainFrame frame;

	AbstractDialog(MainFrame frame, String module)
	{
		super(frame, frame.getTitle() + " - " + module, true);
		
		this.frame = frame;
		
		pnlCenter.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
		pnlSouth.setBorder(BorderFactory.createEmptyBorder(2, 0, 10, 10));
		
		initCenter();
		initSouth();
		
		this.add(pnlCenter, BorderLayout.CENTER);
		this.add(pnlSouth, BorderLayout.SOUTH);
		
		this.setResizable(false);
		this.pack(); 
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}

	abstract void initCenter();
	
	abstract void initSouth();
	
	protected static void alert(Object message, int type)
	{
		JOptionPane.showMessageDialog(null, message, MainFrame.NAME, type);
	}
	
	protected static void handle(Exception e)
	{
		StackTraceElement[] elements = e.getStackTrace();
		JTextArea area = new JTextArea(e.toString());
		
		for(StackTraceElement element : elements)
			area.append("\n\tat " + element.toString());
		
		e.printStackTrace();
		alert("exception occured: " + e, 0);
		alert(new JScrollPane(area), 0);
	}
	
	protected static void alert(Vector<Exception> exceptions)
	{
		String message = "";
		int size = exceptions.size();

		if (size == 1)
			message = exceptions.get(0).getMessage();
		else
		{
			message = "Please check the following fields:";
			for (int i = 0; i < size; i++)
			{
				Exception e = exceptions.get(i);
				message += "\n" + (i + 1) + ". " + e.getMessage();
			}
		}
		alert(message, 2);
	}
	
	protected static String validate(String label, String input, int maxlength, boolean required) throws Exception
	{
		input = input.trim();
		int length = input.length();
		
		if(required && length == 0)
			throw new Exception(label + " is required");
		
		if(maxlength > 0 && length > maxlength)
			throw new Exception(label + " must less than " + maxlength + " characters.");
		 
		return input;
	}
	
	protected static int validate(String label, String input, int minimum, int maximum, boolean hasMinimum, boolean hasMaximum, boolean required) throws Exception
	{
		input = input.trim();
		int value = 0;
		String message = null;
		
		if (required || !input.isEmpty())
		{
			try
			{
				value = Integer.parseInt(input);
				
				if(hasMinimum && value < minimum)
				{
					message = label + "Must be greater than or equals to " + minimum;
				}
				else if(hasMaximum && value > maximum)
				{
					message = label + "Must be less than or equals to " + maximum;
				}
			}
			catch (Exception e)
			{
				message = label + " must be in numerical format.";
			}
			
		}
		
		if (message != null)
			throw new Exception(message);
		
		return value;
	}
}
