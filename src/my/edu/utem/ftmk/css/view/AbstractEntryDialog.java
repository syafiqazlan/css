package my.edu.utem.ftmk.css.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Vector;
import javax.swing.JButton;
import my.edu.utem.ftmk.css.controller.manager.Facade;

public abstract class AbstractEntryDialog extends AbstractDialog
{
	private static final long serialVersionUID = 1L;
	protected JButton btnSubmit;
	protected JButton btnReset;
	protected boolean add;
	
	protected static final GridBagConstraints LABEL = new GridBagConstraints();
	protected static final GridBagConstraints INPUT = new GridBagConstraints();

	static
	{
		INPUT.insets = LABEL.insets = new Insets(5, 0, 5, 0);
		INPUT.fill = LABEL.fill = GridBagConstraints.BOTH;
		INPUT.gridwidth = GridBagConstraints.REMAINDER;
	}
	
	AbstractEntryDialog(MainFrame frame, String module, boolean add)
	{
		super(frame, (add ? "Add" : "Update") + " " +module);
		
		this.add = add;
	}

	@Override
	void initSouth()
	{
		btnSubmit = new JButton("Submit");
		btnReset = new JButton("Reset");
		
		pnlSouth.add(btnSubmit);
		pnlSouth.add(btnReset);
		
		btnSubmit.addActionListener(this);
		btnReset.addActionListener(this);
		
		this.getRootPane().setDefaultButton(btnSubmit);
	}
	
	abstract Vector<Exception> validateInput();
	abstract int add(Facade facade) throws SQLException;
	abstract int update(Facade facade) throws SQLException;
	abstract void reset();
	abstract boolean isModified();
		
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event != null)
		{
			Object source = event.getSource();
			
			if (source == btnSubmit)
			{
				Vector<Exception> exceptions = validateInput();

				if (exceptions == null || exceptions.isEmpty())
				{
					if (isModified())
					{
						try (Facade facade = new Facade()) // class msti
															// aoutocloseable,
															// facade.close will
															// awlays be called
						{
							if (add)
							{
								if (add(facade) != 0)
									reset();
								else
									this.dispose();
							} 
							else // update
							{

								if (update(facade) != 0)
									this.dispose();
							}
						} 
						
						catch (SQLException e)
						{
							if (e instanceof SQLIntegrityConstraintViolationException)
								alert(e.getMessage(), 2);
							else
							handle(e);
						}
					} 
					else
						alert("Please make some changes to update", 2);
				} 
				else
				{
					alert(exceptions);
				}
			} 
			else if (source == btnReset)
			{
				reset();
			}
		}
	}
}
