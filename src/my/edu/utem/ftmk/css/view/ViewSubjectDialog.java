package my.edu.utem.ftmk.css.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Staff;
import my.edu.utem.ftmk.css.model.Subject;

public class ViewSubjectDialog extends AbstractViewDialog implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	private JList<Subject> lstSubjects;
	private JRadioButton rdbSubjectID = new JRadioButton("Subject ID");
	private JRadioButton rdbName = new JRadioButton("Name");

	private JButton btnSearch = new JButton("Search");
	private JTextField txtKeyword = new JTextField();
	private JButton btnClear = new JButton("Clear Search");
	
	public ViewSubjectDialog(MainFrame frame) 
	{
		super(frame, "Subjects");
		
		JPanel pnlNorth = new JPanel(new GridLayout(2, 1));
		JPanel pnlTop = new JPanel();
		JPanel pnlBottom = new JPanel();
		BoxLayout box = new BoxLayout(pnlBottom, BoxLayout.X_AXIS);
		ButtonGroup group = new ButtonGroup();
		
		group.add(rdbSubjectID);
		group.add(rdbName);
		
		pnlBottom.setLayout(box);
		pnlNorth.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
		pnlCenter.setBorder(BorderFactory.createEmptyBorder(2, 15, 0, 15));

		pnlTop.add(new JLabel("Type: "));
		pnlTop.add(rdbSubjectID);
		pnlTop.add(rdbName);
		
		pnlBottom.add(new JLabel("Keyword: "));
		pnlBottom.add(txtKeyword);
		pnlBottom.add(btnSearch);
		
		pnlNorth.add(pnlTop);
		pnlNorth.add(pnlBottom);
		
		btnUpdate.setEnabled(false);
		
		btnSearch.addActionListener(this);
		btnClear.addActionListener(this);
		this.add(pnlNorth, BorderLayout.NORTH);
		
		this.getRootPane().setDefaultButton(btnSearch);
		this.pack();
		this.setVisible(true);
	}
	
	void initCenter()
	{
//		try (Facade facade = new Facade())
//		{
//			lstSubjects = new JList<>(facade.getSubjects());
//		}
//		catch (SQLException e)
//		{
//			lstSubjects = new JList<>();
//			handle(e);
//		}
		
		pnlCenter.add(new JScrollPane(lstSubjects = new JList<>()));
	}

	@Override
	void update()
	{
		Subject subject = lstSubjects.getSelectedValue();
		
		if (subject != null)
		{
			new SubjectEntryDialog(frame, subject);
			this.repaint();
		}
		else
		{
			alert("Please select a subject to update", 2);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if(event != null)
		{
			Object source = event.getSource();
			
			if (source == btnSearch)
			{
				String keyword = txtKeyword.getText().trim().toLowerCase();
				
				int type = rdbSubjectID.isSelected() ? 0 : 1;
				
				if(!keyword.isEmpty())
				{
					try (Facade facade = new Facade())
					{
						Vector<Subject> subjects = facade.getSubjects(keyword, type);
						
						lstSubjects.setListData(subjects);
						
						if(!subjects.isEmpty())
						{
							btnUpdate.setEnabled(true);
						}
						else
						{
							btnUpdate.setEnabled(false);
							txtKeyword.setText("");
							txtKeyword.grabFocus();
							alert("No results found, Please try again with another keyword", 1);
						}
					}
					catch (SQLException e)
					{
						lstSubjects = new JList<>();
						handle(e);
					}					
				}
				else
					alert("Please Enter Keyword", 2);
			}
			else
			{
				super.actionPerformed(event);
			}
		}
	}
}
