package my.edu.utem.ftmk.css.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Clazz;

public class ViewClazzDialog extends AbstractViewDialog implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	private JTable tblclazzs;
	private Vector<Clazz> clazzs;
	private JButton btnAdd = new JButton("Add");

	public ViewClazzDialog(MainFrame frame, Clazz clazz) 
	{
		super(frame, "Classes");	
				
		pnlSouth.add(btnAdd);
		
		btnAdd.addActionListener(this);
		this.setVisible(true);
	}
	
	@Override
	void initCenter()
	{
		
		try (Facade facade = new Facade())
		{
			tblclazzs = new JTable(new ClazzTableModel(clazzs = facade.getClazzs()));
		}
		catch (SQLException e)
		{
			tblclazzs = new JTable();
			handle(e);
		}
		
		pnlCenter.add(new JScrollPane(tblclazzs));
	}
	
	@Override
	void update()
	{		
		int  row = tblclazzs.getSelectedRow();
		
		if(row != -1)
		{
			Clazz clazz = clazzs.get(row);	
			new ClazzEntryDialog(frame, clazz);	
			tblclazzs.setModel(new ClazzTableModel(clazzs));
		}
		else
		{
			alert("Please select a row to update", 2);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if(event != null)
		{
			Object source = event.getSource();
			if(source == btnAdd)
			{
				Clazz clazz = new Clazz();
				
				new ClazzEntryDialog(frame, clazz);
				clazzs.add(clazz);
				Collections.sort(clazzs);
				tblclazzs.setModel(new ClazzTableModel(clazzs));
			}
			else
				super.actionPerformed(event);
		}
	}
}
