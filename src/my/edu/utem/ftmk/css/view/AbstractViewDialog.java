package my.edu.utem.ftmk.css.view;

import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

public abstract class AbstractViewDialog extends AbstractDialog
{
	private static final long serialVersionUID = 1L;
	protected JButton btnUpdate;

	AbstractViewDialog(MainFrame frame, String module)
	{
		super(frame, "View " +module);
	}

	@Override
	void initSouth()
	{
		btnUpdate = new JButton("Update");	
		pnlSouth.add(btnUpdate);	
		btnUpdate.addActionListener(this);		
		this.getRootPane().setDefaultButton(btnUpdate);
	}
	
	abstract void update();
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event != null)
		{
			Object source = event.getSource();
			
			if(source == btnUpdate)
			{
				update();
			}
		}
	}
}