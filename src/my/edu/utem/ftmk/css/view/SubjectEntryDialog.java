package my.edu.utem.ftmk.css.view;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Subject;

public class SubjectEntryDialog extends AbstractEntryDialog implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	private JTextField txtSubjectID;	
	private JTextField txtName; //size: 10 char
	private Subject subject;
	private Subject temp;
	
	public SubjectEntryDialog(MainFrame frame, Subject subject)
	{
		super(frame, frame.getTitle() + " - "+ (subject == null ? "Add" : "Update") +" Subject", true);
		
		this.subject = subject;
		
		this.add = subject == null;
		
		if (!add)
		{
			txtSubjectID.setEnabled(false);
			reset();
		}
		
		this.setVisible(true);
	}
	
	@Override
	void initCenter()
	{
		txtSubjectID = new JTextField(10);
		txtName = new JTextField(10);
		
		JLabel lblID = new JLabel("ID: ");
		JLabel lblName = new JLabel("Name: ");
	
		layout.setConstraints(lblID, LABEL);
		layout.setConstraints(lblName, LABEL);
		
		layout.setConstraints(txtSubjectID, INPUT);
		layout.setConstraints(txtName, LABEL);

		pnlCenter.add(lblID);
		pnlCenter.add(txtSubjectID);
		
		pnlCenter.add(lblName);
		pnlCenter.add(txtName);
	}
	
	@Override
	Vector<Exception> validateInput()
	{
		Vector<Exception> exceptions = new Vector<>();
		temp = new Subject();

		if (add)
		{
			try
			{
				temp.setSubjectID(validate("Subject ID", txtSubjectID.getText(), 10, true));
			}
			catch (Exception e)
			{
				exceptions.add(e);
			}		
		}
		else
			temp.setSubjectID(subject.getSubjectID());

		try
		{
			temp.setName(validate("Subject Name", txtName.getText(), 50, true));
		}
		catch (Exception e)
		{
			exceptions.add(e);
		}
		
		return exceptions;
	}

	@Override
	int add(Facade facade) throws SQLException
	{
		int status = 0;
		try
		{
			status = facade.addSubject(temp);

			if (status != 0)
			{
				alert("Success add a new subject with subject id: " + temp.getSubjectID(), 1);
				this.dispose();
			}

			else
			{
				alert("Unable to add new subject", 2);
				txtName.grabFocus();
			}
		} 
		catch (SQLException e)
		{
			if (e instanceof SQLIntegrityConstraintViolationException)
				throw new SQLIntegrityConstraintViolationException("Subject " + temp + " already exist");
			else
				throw e;
		}
		
		return status;
	}

	@Override
	int update(Facade facade) throws SQLException
	{
		int status = 0;
		
		try
		{
			status = facade.updateSubject(temp);
			
			if(status != 0)
			{
				alert("Subject succesfully updated with subject id: " + subject.getSubjectID(), 1);				
				subject.setName(temp.getName());
			}
			
			else				
				alert("Unable to update this subject with subject id: " + subject.getSubjectID(), 0);		
		}
		catch (SQLException e)
		{
			if (e instanceof SQLIntegrityConstraintViolationException)
				throw new SQLIntegrityConstraintViolationException("Subject " + temp + " already exist");
			else
				throw e;
		}

		return status;
	}

	@Override
	void reset()
	{
		if(add)
		{
			txtName.setText("");
			txtSubjectID.setText("");
		}
		else
		{
			txtName.setText(subject.getName());
			txtSubjectID.setText(subject.getSubjectID());			
		}	
	}

	@Override
	boolean isModified()
	{
		return add ? true : !temp.equals(subject);
	}
}
