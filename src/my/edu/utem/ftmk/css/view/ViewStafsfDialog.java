package my.edu.utem.ftmk.css.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.xml.transform.Source;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Staff;

public class ViewStafsfDialog extends AbstractViewDialog implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	private JTextField txtKeyword = new JTextField();
	private JButton btnFilter = new JButton("Filter");
	private JButton btnClear = new JButton("Clear Filter");
	private JTable tblStaffs;
	private Vector<Staff> staffs;

	public ViewStafsfDialog(MainFrame frame, Staff staff) 
	{
		super(frame, "Staffs");	
				
		staffs.remove(staff);
		tblStaffs.setModel(new StaffTableModel(staffs));
		
		JPanel pnlNorth = new JPanel();
		BoxLayout box = new BoxLayout(pnlNorth, BoxLayout.X_AXIS);
		
		pnlNorth.setLayout(box);
		pnlNorth.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
		pnlCenter.setBorder(BorderFactory.createEmptyBorder(2, 15, 0, 15));

		pnlSouth.add(btnClear);
		pnlNorth.add(new JLabel("Filter Keyword:  "));
		pnlNorth.add(txtKeyword);
		pnlNorth.add(btnFilter);
		
		btnFilter.addActionListener(this);
		btnClear.addActionListener(this);
		this.add(pnlNorth, BorderLayout.NORTH);
		
		this.getRootPane().setDefaultButton(btnFilter);
		this.pack();
		this.setVisible(true);
	}
	
	@Override
	void initCenter()
	{
		tblStaffs = new JTable();
		
		try (Facade facade = new Facade())
		{
			staffs = facade.getStaffs();
		}
		catch (SQLException e)
		{
			handle(e);
		}
		
		pnlCenter.add(new JScrollPane(tblStaffs));
	}
	
	@Override
	void update()
	{		
		int  row = tblStaffs.getSelectedRow();
		
		if(row != -1)
		{
			Vector<Exception> exceptions = new Vector<>();
			String name = null;
			
			int role = (boolean) tblStaffs.getValueAt(row, 2) ? 1 : 0;

			try
			{
				name = validate("Name",tblStaffs.getValueAt(row, 1).toString(), 100, true);
			}
			catch (Exception e)
			{
				exceptions.add(e);
			}
						
			if(exceptions.isEmpty())
			{
				Staff staff = staffs.get(row);	
				Staff temp = new Staff();
				
				temp.setStaffID(staff.getStaffID());
				temp.setPassword(staff.getPassword());
				temp.setName(name);
				temp.setRole(role);

				if (!temp.equals(staff))
				{
					try (Facade facade = new Facade()) //class msti aoutocloseable, facade.close will awlays be called
					{
						int status = facade.updateStaff(temp);
						
						if(status != 0)
						{
							staffs.set(row, temp);
							alert("Success updated a new staff with staff id: " + staff.getStaffID(), 1);				
						}					
						else				
							alert("Unable to update staff with staff id: "+ staff.getStaffID(), 2);		
					} 
					catch (SQLException e)
					{
						handle(e);												
					}
				}
				else
					alert("Please make some changes to update", 2);
			}
			else
				alert(exceptions);
		}
		else
			alert("please select a row to update", 2);
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if(event != null)
		{
			Object source = event.getSource();
			if(source == btnFilter)
			{
				String keyword = txtKeyword.getText().trim().toLowerCase();
				
				if(!keyword.isEmpty())
				{
					Vector<Staff> filtered = new Vector<>();
					
					for (Staff staff : staffs)
						if (staff.getName().toLowerCase().contains(keyword))
							filtered.add(staff);
					
					tblStaffs.setModel(new StaffTableModel(filtered));					
				}
				else
					alert("Please Enter Keyword", 2);
			}
			else if(source == btnClear)
			{
				tblStaffs.setModel(new StaffTableModel(staffs));
				txtKeyword.setText("");		
				txtKeyword.grabFocus();
			}
			else
				super.actionPerformed(event);
		}
	}
}
