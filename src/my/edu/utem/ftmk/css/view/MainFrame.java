/**
 * 
 */
package my.edu.utem.ftmk.css.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Staff;

/**
 * @author MP-
 *
 */
public class MainFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "FTMK UTeM Class Scheduling System"; 
	private JMenuItem mniAddStaff = new JMenuItem("Add Staff");	
	private JMenuItem mniviewStaff = new JMenuItem("View Staffs");
	private JMenuItem mniAddSubject = new JMenuItem("Add Subject");
	private JMenuItem mniAddGrouping = new JMenuItem("Add Grouping");
	private JMenuItem mniViewGrouping = new JMenuItem("View Grouping");
	private JMenuItem mniViewClazz = new JMenuItem("View Class");
	
	private JMenuItem mniViewSubject = new JMenuItem("View Subjects");
	private JMenuItem mniLogout = new JMenuItem("Logout");
	private Staff staff;

	public MainFrame(Staff staff)
	{
		super(NAME);
		
		this.staff = staff;
		
		JMenuBar menubar = new JMenuBar();
		
		if(staff.getRole() == 1) //admin
		{
			JMenu menuStaff = new JMenu("Staff");
			JMenu menuSubject = new JMenu("Subject");
			JMenu menuGrouping = new JMenu("Grouping");
			JMenu menuClass = new JMenu("Class");
			JMenu menuVenue = new JMenu("Venue");
			
			menubar.add(menuStaff);
			menubar.add(menuSubject);
			menubar.add(menuGrouping);
			menubar.add(menuClass);
			menubar.add(menuVenue);
			menuSubject.add(mniAddSubject);
			menuSubject.add(mniViewSubject);
			
			menuClass.add(mniViewClazz);
			
			menuGrouping.add(mniAddGrouping);
			menuGrouping.add(mniViewGrouping);
			
			menuStaff.add(mniAddStaff);
			menuStaff.add(mniviewStaff);
		}
		
		JMenu menuSchedule = new JMenu("Schedule");

		JMenu menuAccount = new JMenu("Account");
		menuAccount.add(mniLogout);
		
		menubar.add(menuSchedule);	
		menubar.add(menuAccount);	
		
		mniViewClazz.addActionListener(this);
		mniViewGrouping.addActionListener(this);
		mniAddGrouping.addActionListener(this);
		mniAddStaff.addActionListener(this);
		mniviewStaff.addActionListener(this);	
		mniAddSubject.addActionListener(this);
		mniViewSubject.addActionListener(this);
		mniLogout.addActionListener(this);
		menubar.add(menuAccount);
		
		this.add(new JLabel("Welcome: "+ staff.getName(), JLabel.CENTER));
		
		this.setResizable(false);
		this.setSize(600, 300);
		this.setJMenuBar(menubar);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null); //appear at center of screen
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if (event != null)
		{
			Object source = event.getSource();
			
			if(source == mniAddStaff)
			{
				new AddStaffDialog(this);
			}
			else if(source == mniviewStaff)
			{
				new ViewStafsfDialog(this, staff);
			}
			else if(source == mniAddSubject)
			{
				new SubjectEntryDialog(this, null);
			}
			else if(source == mniViewSubject)
			{
				new ViewSubjectDialog(this);
			}
			else if(source == mniLogout)
			{
				Facade.logout();
				new LoginFrame();
				this.dispose();
			}
			else if(source == mniAddGrouping)
			{
				new GroupingEntryDialog(this, null);
			}
			else if(source == mniViewGrouping)
			{
				new ViewGroupingDialog(this);
			}
			else if(source == mniViewClazz)
			{
				new ViewClazzDialog(this, null);
			}
		}
	}
}
