package my.edu.utem.ftmk.css.view;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JScrollPane;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Grouping;

public class ViewGroupingDialog extends AbstractViewDialog implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	private JList<Grouping> lstGrouping;
	private Vector<Grouping> groupings;
	
	public ViewGroupingDialog(MainFrame frame) 
	{
		super(frame, "Grouping");
		this.setVisible(true);
	}
	
	void initCenter()
	{
		lstGrouping = new JList<>();
		
		try (Facade facade = new Facade())
		{
			lstGrouping.setListData(groupings = facade.getGroupings());
		}
		catch (SQLException e)
		{
			lstGrouping = new JList<>();
			handle(e);
		}
		
		lstGrouping.setPreferredSize(new Dimension(300, 200));
		pnlCenter.add(new JScrollPane(lstGrouping));
	}

	@Override
	void update()
	{
		Grouping grouping = lstGrouping.getSelectedValue();
		
		if (grouping != null)
		{
			new GroupingEntryDialog(frame, grouping);
			
			Collections.sort(groupings);
			lstGrouping.setListData(groupings);
			this.repaint();
		}
		else
		{
			alert("Please select a group to update", 2);
		}
	}
}
