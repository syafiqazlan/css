package my.edu.utem.ftmk.css.view;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import my.edu.utem.ftmk.css.model.Staff;

public class StaffTableModel extends DefaultTableModel 
{
	private static final long serialVersionUID = 1L;
	private static final Class<?>[] classes = new Class[] {String.class, String.class, Boolean.class};
	
	public StaffTableModel(Vector<Staff> staffs)
	{
		int size = staffs.size();
		
		Object[][] data = new Object[size][3];
		
		for (int i =0; i<size; i++)
		{
			Staff staff = staffs.get(i);
			data[i][0] = staff.getStaffID();
			data[i][1] = staff.getName();
			data[i][2] = staff.getRole() == 1;
		}
		
		setDataVector(data, new String[] {"Staff ID", "Name", "Administrator"});
	}

	@Override
	public Class<?> getColumnClass(int columnIndex)
	{
		return classes[columnIndex];
	}
	
	@Override
	public boolean isCellEditable(int row, int column) 
	{
		return column != 0;
	}
}
