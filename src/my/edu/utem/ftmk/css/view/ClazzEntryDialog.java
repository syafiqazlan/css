package my.edu.utem.ftmk.css.view;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Clazz;
import my.edu.utem.ftmk.css.model.Grouping;
import my.edu.utem.ftmk.css.model.Staff;
import my.edu.utem.ftmk.css.model.Subject;

public class ClazzEntryDialog extends AbstractEntryDialog
{

	private static final long serialVersionUID = 1L;
	private JComboBox<String> cmbType;
	private JComboBox<Staff> cmbStaffID; //size: 10 char
	private JComboBox<Subject> cmbSubjectID;
	private JComboBox<Grouping> cmbGroupingID;
	
	private Vector<Staff> staffs;
	private Vector<Subject> subjects;
	private Vector<Grouping> groupings;
	
	private Clazz clazz;
	private Clazz temp;
	
	public ClazzEntryDialog(MainFrame frame, Clazz clazz)
	{
		super(frame, "Class", clazz.getClazzID() == 0);
		
		this.clazz = clazz;

		
		if (!add)
		{
			//txtYearOfStudy.setEnabled(false);
			reset();
		}
		
		this.setVisible(true);
	}
	
	@Override
	void initCenter()
	{		
		JLabel lblType = new JLabel("Type: ");
		JLabel lblStaff = new JLabel("Staff: ");
		JLabel lblSubjectID = new JLabel("Subject: ");
		JLabel lblGroupID = new JLabel("Group: ");
	
		cmbType = new JComboBox<>(new String[] {"Lecture", "Lab"});
		cmbStaffID = new JComboBox<>();
		cmbSubjectID = new JComboBox<>();
		cmbGroupingID = new JComboBox<>();
		
		
		try (Facade facade = new Facade())
		{
			cmbStaffID.setModel(new DefaultComboBoxModel<>(staffs = facade.getStaffs()));
			cmbSubjectID.setModel(new DefaultComboBoxModel<>(subjects = facade.getSubjects()));
			cmbGroupingID.setModel(new DefaultComboBoxModel<>(groupings = facade.getGroupings()));
		}
		catch (SQLException e)
		{
			
		}
		
		layout.setConstraints(lblType, LABEL);
		layout.setConstraints(lblStaff, LABEL);
		
		layout.setConstraints(lblSubjectID, LABEL);
		layout.setConstraints(lblGroupID, LABEL);

		layout.setConstraints(cmbType, INPUT);
		layout.setConstraints(cmbStaffID, INPUT);
		layout.setConstraints(cmbSubjectID, INPUT);
		layout.setConstraints(cmbGroupingID, INPUT);
		
		pnlCenter.add(lblType);
		pnlCenter.add(cmbType);
		
		pnlCenter.add(lblStaff);
		pnlCenter.add(cmbStaffID);

		pnlCenter.add(lblSubjectID);
		pnlCenter.add(cmbSubjectID);

		pnlCenter.add(lblGroupID);
		pnlCenter.add(cmbGroupingID);
	}
	
	@Override
	Vector<Exception> validateInput()
	{
		temp = new Clazz();

		if (!add)
			temp.setClazzID(clazz.getClazzID());
		
		Staff staff = (Staff) cmbStaffID.getSelectedItem();
		Subject subject = (Subject) cmbSubjectID.getSelectedItem();
		Grouping grouping = (Grouping) cmbGroupingID.getSelectedItem();
		
		temp.setType(cmbType.getSelectedIndex() + 1);
		temp.setStaffID(staff.getStaffID());
		temp.setSubjectID(subject.getSubjectID());
		temp.setGroupingID(grouping.getGroupingID());
		
		temp.setStaffName(staff.getName());
		temp.setSubjectName(subject.getName());
		temp.setGroupingName(grouping.toString());

		return null;
	}

	private void copy()
	{
		clazz.setClazzID(temp.getClazzID());
		clazz.setType(temp.getType());
		clazz.setStaffID(temp.getStaffID());
		clazz.setSubjectID(temp.getSubjectID());
		clazz.setGroupingID(temp.getGroupingID());
		clazz.setStaffName(temp.getStaffName());
		clazz.setSubjectName(temp.getSubjectName());
		clazz.setGroupingName(temp.getGroupingName());
	}
	@Override
	int add(Facade facade) throws SQLException
	{
		try
		{
			int status = facade.addClazz(temp);
			 
			if(status != 0)
				{
					alert("Success add a new class with class id: " + temp.getClazzID(),1);	
					copy();
				}
				
				else
				{
					alert("Unable to add new class", 2);
				}
		}
		catch (SQLException e)
		{
			if(e instanceof SQLIntegrityConstraintViolationException)
			{
				e.printStackTrace();
				throw new SQLIntegrityConstraintViolationException("Clazz "+ temp +" already exist");
			}
			else
				throw e;
		}
		return 0;
	}

	@Override
	int update(Facade facade) throws SQLException
	{
		int status = 0;
		
		try
		{
		status = facade.updateClazz(temp);

			if(status != 0)
			{
				alert("Grouping succesfully updated with clazz id: " + temp, 1);	
				clazz.setType(temp.getType());
				clazz.setStaffID(temp.getStaffID());
				clazz.setSubjectID(temp.getSubjectID());
				clazz.setGroupingID(temp.getGroupingID());
			}
			
			else				
				alert("Unable to update this group with Grouping id: " + clazz.getClazzID(), 2);
		}
		catch (SQLException e)
		{
			if (e instanceof SQLIntegrityConstraintViolationException)
				alert(e.getMessage(), 2);
			else
			handle(e);
		}
		
		return status;
	}

	@Override
	void reset()
	{
		if(add)
		{
			cmbType.setSelectedIndex(0);
			cmbStaffID.setSelectedIndex(0);
			cmbSubjectID.setSelectedIndex(0);
			cmbGroupingID.setSelectedIndex(0);
		}
		else
		{
			cmbType.setSelectedIndex(clazz.getType() - 1);

			for (Staff staff : staffs)
				if (staff.getStaffID().equals(clazz.getStaffID()))
					cmbStaffID.setSelectedItem(staff);
			
			for (Subject subject : subjects)
				if (subject.getSubjectID().equals(clazz.getSubjectID()))
					cmbSubjectID.setSelectedItem(subject);
			
			for (Grouping grouping : groupings)
				if (grouping.getGroupingID() == clazz.getGroupingID())
					cmbGroupingID.setSelectedItem(grouping);
		}		
	}

	@Override
	boolean isModified()
	{
		return add ? true : !temp.equals(clazz);
	}
}
