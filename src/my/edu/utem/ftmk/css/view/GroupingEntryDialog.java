package my.edu.utem.ftmk.css.view;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Grouping;

public class GroupingEntryDialog extends AbstractEntryDialog
{

	private static final long serialVersionUID = 1L;
	private JTextField txtYearOfStudy;
	private JComboBox<String> cmbCourse; //size: 10 char
	private JSpinner spnSectionNo;
	private JSpinner spnGroupNo;
	private Grouping grouping;
	private Grouping temp;
	
	public GroupingEntryDialog(MainFrame frame, Grouping grouping)
	{
		super(frame, "Grouping", grouping == null);
		
		this.grouping = grouping;
		
		this.add = grouping == null;
		
		if (!add)
		{
			//txtYearOfStudy.setEnabled(false);
			reset();
		}
		
		this.setVisible(true);
	}
	
	@Override
	void initCenter()
	{		
		JLabel lblYearOfStudy = new JLabel("Year: ");
		JLabel lblCourse = new JLabel("Course: ");
		JLabel lblSectionNo = new JLabel("Section: ");
		JLabel lblGroupNo = new JLabel("Group: ");
	
		txtYearOfStudy = new JTextField(20);
		cmbCourse = new JComboBox<>(new String[] {"BITC", "BITD", "BITE", "BITI", "BITM", "BITS", "BITZ"});
		spnSectionNo = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
		spnGroupNo = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
		
		layout.setConstraints(lblYearOfStudy, LABEL);
		layout.setConstraints(lblCourse, LABEL);
		
		layout.setConstraints(lblSectionNo, LABEL);
		layout.setConstraints(lblGroupNo, LABEL);

		layout.setConstraints(txtYearOfStudy, INPUT);
		layout.setConstraints(cmbCourse, INPUT);
		layout.setConstraints(spnSectionNo, INPUT);
		layout.setConstraints(spnGroupNo, INPUT);
		
		pnlCenter.add(lblYearOfStudy);
		pnlCenter.add(txtYearOfStudy);
		
		pnlCenter.add(lblCourse);
		pnlCenter.add(cmbCourse);

		pnlCenter.add(lblSectionNo);
		pnlCenter.add(spnSectionNo);

		pnlCenter.add(lblGroupNo);
		pnlCenter.add(spnGroupNo);
	}
	
	@Override
	Vector<Exception> validateInput()
	{
		Vector<Exception> exceptions = new Vector<>();
		temp = new Grouping();

		if (!add)
			temp.setGroupingID(grouping.getGroupingID());
		
		temp.setCourse((String) cmbCourse.getSelectedItem());
		temp.setSectionNo((int) spnSectionNo.getValue());
		temp.setGroupNo((int) spnGroupNo.getValue());
		
		try
		{
			temp.setYearOfStudy(validate("Year of study", txtYearOfStudy.getText(), 1, 10, true, true, true));
		}
		catch (Exception e)
		{
			exceptions.add(e);
		}
		
		return exceptions;
	}

	@Override
	int add(Facade facade) throws SQLException
	{
		int status = 0;
		
		try
		{
			status = facade.addGrouping(temp);
			 
			if(status != 0)
				{
					alert("Success add a new group with group id: " + temp.getGroupingID(),1);				
					//this.dispose();
				}
				
				else
				{
					alert("Unable to add new group", 2);
					txtYearOfStudy.grabFocus();		
				}
		}
		catch (SQLException e)
		{
			if(e instanceof SQLIntegrityConstraintViolationException)
				throw new SQLIntegrityConstraintViolationException("Group "+ temp +" already exist");
			else
				throw e;
		}
		

		return status;
	}

	@Override
	int update(Facade facade) throws SQLException
	{
		int status = 0;
		
		try
		{
		status = facade.updateGrouping(temp);

			if(status != 0)
			{
				alert("Grouping succesfully updated with Grouping id: " + temp, 1);	
				grouping.setYearOfStudy(temp.getYearOfStudy());
				grouping.setCourse(temp.getCourse());
				grouping.setSectionNo(temp.getSectionNo());
				grouping.setGroupNo(temp.getGroupNo());
			}
			
			else				
				alert("Unable to update this group with Grouping id: " + grouping.getGroupingID(), 2);
		}
		catch (SQLException e)
		{
			if (e instanceof SQLIntegrityConstraintViolationException)
				alert(e.getMessage(), 2);
			else
			handle(e);
		}
		
		return status;
	}

	@Override
	void reset()
	{
		if(add)
		{
			txtYearOfStudy.setText("");
			cmbCourse.setSelectedIndex(0);
			spnGroupNo.setValue(1);
			spnSectionNo.setValue(1);
		}
		else
		{
			txtYearOfStudy.setText(String.valueOf(grouping.getYearOfStudy()));
			cmbCourse.setSelectedItem(grouping.getCourse());
			spnGroupNo.setValue(grouping.getGroupNo());
			spnSectionNo.setValue(grouping.getSectionNo());		
		}	
		
		txtYearOfStudy.grabFocus();
	}

	@Override
	boolean isModified()
	{
		return add ? true : !temp.equals(grouping);
	}
}
