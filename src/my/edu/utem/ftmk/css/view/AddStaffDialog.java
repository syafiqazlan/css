package my.edu.utem.ftmk.css.view;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Staff;

public class AddStaffDialog extends AbstractEntryDialog implements ActionListener 
{
	private static final long serialVersionUID = 1L;

	private JTextField txtName;
	private JCheckBox chkRole;
	private JTextField txtStaffID;
	private JPasswordField txtPassword;
	private Staff staff;
	
	public AddStaffDialog(MainFrame frame)
	{
		super(frame,"Staff", true);//true = add
		this.setVisible(true);
	}
	
	@Override
	void initCenter()
	{		
		JLabel lblStaffID = new JLabel("Staff ID: ");
		JLabel lblPassword = new JLabel("Password: ");
		JLabel lblName = new JLabel("Name: ");
		JLabel lblRole = new JLabel("Role: ");
	
		txtStaffID = new JTextField(20);
		txtPassword = new JPasswordField(20);
		txtName = new JTextField(20);
		chkRole = new JCheckBox("Administrator");
		
		layout.setConstraints(lblStaffID, LABEL);
		layout.setConstraints(lblPassword, LABEL);
		layout.setConstraints(lblName, LABEL);
		layout.setConstraints(lblRole, LABEL);
		
		layout.setConstraints(txtStaffID, INPUT);
		layout.setConstraints(txtPassword, INPUT);
		layout.setConstraints(txtName, INPUT);
		layout.setConstraints(chkRole, INPUT);
		
		pnlCenter.add(lblStaffID);
		pnlCenter.add(txtStaffID);
		
		pnlCenter.add(lblPassword);
		pnlCenter.add(txtPassword);	

		pnlCenter.add(lblName);
		pnlCenter.add(txtName);
		
		pnlCenter.add(lblRole);
		pnlCenter.add(chkRole);	
	}
	
	@Override
	Vector<Exception> validateInput()
	{
		Vector<Exception> exceptions = new Vector<>();
		staff = new Staff();
		staff.setRole(chkRole.isSelected() ? 1 : 0);
		
		try
		{
			staff.setStaffID(validate("Staff ID", txtStaffID.getText(), 20, true));
		}
		catch (Exception e)
		{
			exceptions.add(e);
		}

		try
		{
			staff.setPassword(validate("Password", new String (txtPassword.getPassword()), 20, true));
		}
		catch (Exception e)
		{
			exceptions.add(e);
		}

		try
		{
			staff.setName(validate("Name", txtName.getText(), 100, true));
		}
		catch (Exception e)
		{
			exceptions.add(e);
		}
		
		return exceptions;
	}

	@Override
	int add(Facade facade) throws SQLException
	{
		int status = facade.addStaff(staff);
		
		if(status != 0)
		{
			alert("Success add a new staff with staff id: " + staff.getStaffID() + ".", 1);	//1 = info icon			
			this.dispose();
		}
		
		else				
			alert("Unable to add new staff.", 2); //2 = warning icon
				
		return status;
	}

	@Override
	int update(Facade facade) throws SQLException
	{
		return 0;
	}

	@Override
	void reset()
	{
		txtStaffID.setText("");
		txtPassword.setText("");
		txtName.setText("");
		chkRole.setSelected(false);	
	}

	@Override
	boolean isModified()
	{
		return true;
	}
}
