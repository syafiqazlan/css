/**
 * 
 */
package my.edu.utem.ftmk.css.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Staff;

/**
 * @author MP-
 *
 */
public class LoginFrame extends JFrame implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	private JTextField txtStaffID = new JTextField(10); //size: 10 char
	private JPasswordField txtPassword = new JPasswordField(10);
	private JButton btnSubmit = new JButton("Submit");
	private JButton btnReset = new JButton("Reset");
	
	public LoginFrame()
	{
		super(MainFrame.NAME);
		
		GridBagLayout layout = new GridBagLayout();
				
		JPanel pnlCenter = new JPanel(layout);
		JPanel pnlSouth = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		pnlCenter.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
		pnlSouth.setBorder(BorderFactory.createEmptyBorder(2, 0, 10, 10));
		
		JLabel lblStaffID = new JLabel("Staff ID: ");
		JLabel lblPassword = new JLabel("Password: ");
		
		layout.setConstraints(lblStaffID, AbstractEntryDialog.LABEL);
		layout.setConstraints(lblPassword, AbstractEntryDialog.LABEL);
		layout.setConstraints(txtStaffID, AbstractEntryDialog.INPUT);
		layout.setConstraints(txtPassword, AbstractEntryDialog.INPUT);
	
		pnlCenter.add(lblStaffID);
		pnlCenter.add(txtStaffID);
		
		pnlCenter.add(lblPassword);
		pnlCenter.add(txtPassword);	
		
		pnlSouth.add(btnSubmit);
		pnlSouth.add(btnReset);
		
		btnSubmit.addActionListener(this);
		btnReset.addActionListener(this);
		
		this.add(pnlCenter, BorderLayout.CENTER);
		this.add(pnlSouth, BorderLayout.SOUTH);
		
		this.getRootPane().setDefaultButton(btnSubmit);
		this.setResizable(false);
		this.pack(); //set frame size to fit components
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null); //appear at center of screen
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if (event != null)
		{
			Object source = event.getSource();
			
			if (source == btnSubmit)
			{
				Vector<Exception> exceptions = new Vector<>();
				
				String staffID = null;
				String password = null;
				
				try
				{
					staffID = AbstractDialog.validate("Staff ID", txtStaffID.getText(), -1, true);//-1 = unlimited
				}
				catch (Exception e)
				{
					exceptions.add(e);
				}
				
				try
				{
					password = AbstractDialog.validate("Password", new String (txtPassword.getPassword()), -1, true);//-1 = unlimited
				}
				catch (Exception e)
				{
					exceptions.add(e);
				}	
								
				if (exceptions.isEmpty()) //no exceptions
				{
					try (Facade facade = new Facade()) //class msti aoutocloseable, facade.close will awlays be called
					{
						Staff staff = facade.getStaff(staffID, password);
						
						if(staff != null)
						{
							new MainFrame(staff);
							this.dispose();
						}
						
						else				
						{
							AbstractDialog.alert("Invalid staff ID and/or password", 2);
							txtStaffID.grabFocus();					
						}
					} 
					catch (SQLException e)
					{
						e.printStackTrace();
						AbstractDialog.handle(e);
					}		
				}
				else
				{
					AbstractDialog.alert(exceptions);
				}	
			}
			else if (source == btnReset)
			{
				txtStaffID.setText("");
				txtPassword.setText("");
				txtStaffID.grabFocus();
			}
		}
	}

	public static void main(String[] args) throws Exception
	{
		//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		UIManager.setLookAndFeel(new NimbusLookAndFeel());

		new LoginFrame();
	}

}
