package my.edu.utem.ftmk.css.view;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import my.edu.utem.ftmk.css.model.Clazz;

public class ClazzTableModel extends DefaultTableModel 
{
	private static final long serialVersionUID = 1L;
	
	public ClazzTableModel(Vector<Clazz> Clazzs)
	{
		int size = Clazzs.size();
		
		Object[][] data = new Object[size][4];
		
		for (int i =0; i<size; i++)
		{
			Clazz Clazz = Clazzs.get(i);
			data[i][0] = Clazz.getType() == 1 ? "Lecture" : "Lab";
			data[i][1] = Clazz.getStaffName();
			data[i][2] = Clazz.getSubjectName();
			data[i][3] = Clazz.getGroupingName();
		}
		
		setDataVector(data, new String[] {"Type Of Class", "Staff", "Subject", "Grouping"});
	}
	
	@Override
	public boolean isCellEditable(int row, int column) 
	{
		return false;
	}
}
