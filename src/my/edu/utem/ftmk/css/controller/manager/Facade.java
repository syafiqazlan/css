package my.edu.utem.ftmk.css.controller.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;

import my.edu.utem.ftmk.css.model.Clazz;
import my.edu.utem.ftmk.css.model.Grouping;
import my.edu.utem.ftmk.css.model.Staff;
import my.edu.utem.ftmk.css.model.Subject;

public class Facade implements AutoCloseable
{
	private static Staff staff;
	private Connection connection;
	private StaffManager staffManager;
	private SubjectManager subjectManager;
	private GroupingManager groupingManager;
	private ClazzManager clazzManager;

	static
	{
		// Load the database driver
		try
		{
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("Driver not found");
			System.exit(0);
		}
	}
	
	private void isAllowed(int... roles) throws SQLException
	{
		boolean allowed = false;
		
		if (staff != null && roles.length != 0)
			for (int role : roles)
				if (role == staff.getRole())
					allowed = true;
		
		if (!allowed)
			throw new SQLException("You're not authorized to access this method");
	}
	
	private Connection getConnection() throws SQLException
	{
		// Connect to the database using the specified URL, user ID, and password
		if (connection == null || connection.isClosed())
		{
			connection = DriverManager.getConnection("jdbc:derby://localhost:1527/dbCSS", "admin", "123");
			connection.setAutoCommit(false);
		}
		return connection;
	}
	
	private StaffManager getStaffManager()
	{
		if (staffManager == null)
			staffManager = new StaffManager(this);
		
		return staffManager;
	}
	
	private ClazzManager getClazzManager()
	{
		if (clazzManager == null)
			clazzManager = new ClazzManager(this);
		
		return clazzManager;
	}
	
	private SubjectManager getSubjectManager()
	{
		if (subjectManager == null)
			subjectManager = new SubjectManager(this);
		
		return subjectManager;
	}
	
	private GroupingManager getGroupingManager()
	{
		if (groupingManager == null)
			groupingManager = new GroupingManager(this);
		
		return groupingManager;
	}	
	
	PreparedStatement prepareStatement(String query) throws SQLException
	{
		return getConnection().prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
	}
	
	public static void logout()
	{
		staff = null;
	}
	
	public void close() throws SQLException
	{
		if(connection != null && !connection.isClosed())
		{
			connection.commit();
			connection.close();
		}
			
	}
	
	public int addStaff(Staff staff) throws SQLException
	{
		isAllowed(1);
		return getStaffManager().addStaff(staff);
	}
	
	public int updateStaff(Staff staff) throws SQLException
	{
		isAllowed(1);
		return getStaffManager().updateStaff(staff);
	}
	
	public Staff getStaff(String staffID, String password) throws SQLException
	{
		return staff = getStaffManager().getStaff(staffID, password);
	}
	
	public Vector<Staff> getStaffs() throws SQLException
	{
		isAllowed(1);
		return getStaffManager().getStaffs();
	}
	
	public int addSubject(Subject subject) throws SQLException
	{
		isAllowed(1);
		return getSubjectManager().addSubject(subject);
	}
	
	public int updateSubject(Subject subject) throws SQLException
	{
		isAllowed(1);
		return getSubjectManager().updateSubject(subject);
	}
	
	public Vector<Subject> getSubjects() throws SQLException
	{
		isAllowed(1);
		return getSubjectManager().getSubjects();
	}
	
	public Vector<Subject> getSubjects(String keyword, int type) throws SQLException
	{
		isAllowed(1);
		return getSubjectManager().getSubjects(keyword, type);
	}
	
	public int addGrouping(Grouping grouping) throws SQLException
	{
		isAllowed(1);
		return getGroupingManager().addGrouping(grouping);
	}
	
	public int updateGrouping(Grouping grouping) throws SQLException
	{
		isAllowed(1);
		return getGroupingManager().updateGrouping(grouping);
	}
	
	public Vector<Grouping> getGroupings() throws SQLException
	{
		isAllowed(1);
		return getGroupingManager().getGroupings();
	}
	
	public Vector<Grouping> getGroupings(String keyword, int type) throws SQLException
	{
		isAllowed(1);
		return getGroupingManager().getGroupings(keyword, type);
	}
	//ass
	
	public int addClazz(Clazz Clazz) throws SQLException
	{
		isAllowed(1);
		return getClazzManager().addClazz(Clazz);
	}
	
	public int updateClazz(Clazz Clazz) throws SQLException
	{
		isAllowed(1);
		return getClazzManager().updateClazz(Clazz);
	}
	
	public Vector<Clazz> getClazzs() throws SQLException
	{
		isAllowed(1);
		return getClazzManager().getClazzs();
	}
	
	public Vector<Clazz> getClazzs(String keyword, int type) throws SQLException
	{
		isAllowed(1);
		return getClazzManager().getClazzs(keyword, type);
	}
}