package my.edu.utem.ftmk.css.controller.manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import my.edu.utem.ftmk.css.model.Staff;

/**
 * Manage staff data on staff table
 * @author Syafiq Azlan
 * @version 1.0
 */

class StaffManager extends AbstractTablemanager
{
	StaffManager(Facade facade)
	{
		super(facade);
	}
	
	private void setStaff(PreparedStatement ps, Staff staff, int start) throws SQLException
	{
		ps.setString(start, staff.getPassword());
		ps.setString(start + 1, staff.getName());
		ps.setInt(start + 2, staff.getRole());	
	}
	
	private Staff getStaff(ResultSet rs) throws SQLException
	{
		//declare an object to hold the result
		Staff staff = new Staff();
		
		//read result
		staff.setStaffID(rs.getString(1));
		staff.setPassword(rs.getString(2));
		staff.setName(rs.getString(3));
		staff.setRole(rs.getInt("Role"));

		return staff;
	}
	
	int addStaff(Staff staff) throws SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("INSERT INTO Staff Values (?, ?, ?, ?)");
		
		ps.setString(1, staff.getStaffID());
		setStaff(ps, staff, 2);

		//return status
		return ps.executeUpdate();
	}

	int updateStaff(Staff staff) throws SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("UPDATE Staff set Password = ?, Name = ?, Role = ? where StaffID = ?");
		
		setStaff(ps, staff, 1);
		ps.setString(4, staff.getStaffID());

		//return status
		return ps.executeUpdate();
	}

	Vector<Staff> getStaffs() throws SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("select * from staff order by StaffID");
		
		//send statement and retrieve result
		ResultSet rs = ps.executeQuery();
		
		//create a list to store result
		Vector<Staff> staffs = new Vector<>();
		
		//read result
		while (rs.next())
			staffs.add(getStaff(rs));
		
		//return status
		return staffs;
	}
	
	//login
	Staff getStaff(String staffID, String password) throws SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("select * from staff where StaffID = ? and Password = ?");
		
		//set values
		ps.setString(1,  staffID);
		ps.setString(2, password);
		
		//send statement and retrieve result
		ResultSet rs = ps.executeQuery();
		
		//declare object to hold result
		Staff staff = null;
		
		//read result check is rs return some rows
		if (rs.next())
			staff = getStaff(rs);
		
		//return object
		return staff;
	}
}
