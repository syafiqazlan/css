package my.edu.utem.ftmk.css.controller.manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import my.edu.utem.ftmk.css.model.Subject;

class SubjectManager extends AbstractTablemanager
{	
	SubjectManager(Facade facade)
	{
		super(facade);
	}

	private void setSubject(PreparedStatement ps, Subject subject, int start) throws SQLException
	{
		ps.setString(start, subject.getName());
	}
	
	private Subject getSubject(ResultSet rs) throws SQLException
	{
		//declare an object to hold the result
		Subject subject = new Subject();
		
		//read result
		subject.setSubjectID(rs.getString(1));
		subject.setName(rs.getString(2));
		
		return subject;
	}
	
	int addSubject(Subject subject) throws SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("INSERT INTO Subject Values (?, ?)");
		
		ps.setString(1, subject.getSubjectID());	
		setSubject(ps, subject, 2);

		//return status
		return ps.executeUpdate(); 
	}

	int updateSubject(Subject subject) throws SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("UPDATE Subject set name = ? where subjectID = ?");
		
		ps.setString(2, subject.getSubjectID());
		setSubject(ps, subject, 1);

		//return status
		return  ps.executeUpdate(); 
	}
	
	private Vector<Subject> getSubjects(PreparedStatement ps) throws SQLException
	{
		//send statement and retrieve result
		ResultSet rs = ps.executeQuery();
		
		//create a list to store result
		Vector<Subject> subjects = new Vector<>();
		
		//read result
		while (rs.next())
			subjects.add(getSubject(rs));

		//return status
		return subjects;
	}
	
	Vector<Subject> getSubjects() throws SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("select * from Subject order by SubjectID");
		
		return getSubjects(ps);
	}
	
	Vector<Subject> getSubjects(String keyword, int type) throws SQLException
	{
		PreparedStatement ps = facade.prepareStatement("select * from Subject where LOWER("+ (type == 0 ? "SubjectID" : "name") + ") like ? order by SubjectID");		
		ps.setString(1, "%" + keyword.toLowerCase() + "%");

		return getSubjects(ps);
	}
}
