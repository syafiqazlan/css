/**
 * 
 */
package my.edu.utem.ftmk.css.controller.manager;

/**
 * @author MP-
 *
 */
abstract class AbstractTablemanager 
{
	protected Facade facade; //only subclasses can access
	
	AbstractTablemanager(Facade facade)
	{
		this.facade = facade;
	}
}
