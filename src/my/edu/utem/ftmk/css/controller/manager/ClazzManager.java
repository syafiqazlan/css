/**
 * 
 */
package my.edu.utem.ftmk.css.controller.manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import my.edu.utem.ftmk.css.model.Clazz;

/**
 * This class is used to manage the Clazz data on the Clazz table.
 * 
 * @author Satrya Fajri Pratama
 * @version 1.0
 */
class ClazzManager extends AbstractTablemanager
{
	ClazzManager(Facade facade)
	{
		super(facade);
	}
	
	private void setClazz(PreparedStatement ps, Clazz clazz, boolean add) throws SQLException
	{
		ps.setInt(1, clazz.getType());
		ps.setString(2, clazz.getStaffID());
		ps.setString(3, clazz.getSubjectID());
		ps.setInt(4, clazz.getGroupingID());
		
		if(!add)
			ps.setInt(5, clazz.getClazzID());
	}
	
	int addClazz(Clazz clazz) throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("INSERT INTO Clazz (Type, StaffID, SubjectID, GroupingID)VALUES (?, ?, ?, ?)");
		
		setClazz(ps,clazz, true);
		
		int status = ps.executeUpdate();
				
				if(status != 0)
				{
					ResultSet rs = ps.getGeneratedKeys();
					if (rs.next())
						clazz.setClazzID(rs.getInt(1));
				}
		// Return the status
		return status;
	}
	
	int updateClazz(Clazz clazz) throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("UPDATE Clazz SET Type = ?, StaffID = ?, SubjectID = ?, GroupingID = ? WHERE ClazzID = ?");
		
		setClazz(ps,clazz, false);
		
		// Return the status
		return ps.executeUpdate();
	}
	
	private Vector<Clazz> getClazzs(PreparedStatement ps) throws SQLException
	{
		// Send the statement and retrieve the results
		ResultSet rs = ps.executeQuery();
		
		// Create a list to store the results
		Vector<Clazz> Clazzs = new Vector<>();

		// Read the results
		while (rs.next())
		{
			Clazz clazz = new Clazz();
			
			clazz.setClazzID(rs.getInt(1));
			clazz.setType(rs.getInt(2));
			clazz.setStaffID(rs.getString(3));
			clazz.setSubjectID(rs.getString(4));
			clazz.setGroupingID(rs.getInt(5));

			clazz.setStaffName(rs.getString(6));
			clazz.setSubjectName(rs.getString(7));
			clazz.setGroupingName(rs.getString(8) + rs.getString(9) + "S" + rs.getInt(10) + "/G" + rs.getInt(11));

			Clazzs.add(clazz);
		}
		
		// Return the status
		return Clazzs;
	}
	
	Vector<Clazz> getClazzs() throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("SELECT c.*, f.Name, j.name, g.YearOfStudy, g.Course, g.SectionNo, g.GroupNo FROM Clazz c, Staff f, Subject j, Grouping g WHERE c.StaffID = f.StaffID AND c.SubjectID = j.SubjectID and c.GroupingID = g.GroupingID ORDER BY c.Type, f.name, j.name, g.YearOfStudy, g.Course, g.SectionNo, g.GroupNo");
		
		return getClazzs(ps);
	}
	
	Vector<Clazz> getClazzs(String keyword, int type) throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("SELECT * FROM Clazz WHERE LOWER(" + (type == 0 ? "ClazzID" : "Name") + ") LIKE ? ORDER BY ClazzID");
		
		ps.setString(1, "%" + keyword.toLowerCase() + "%");
		
		return getClazzs(ps);
	}
}