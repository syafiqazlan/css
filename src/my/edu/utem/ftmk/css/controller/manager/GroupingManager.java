/**
 * 
 */
package my.edu.utem.ftmk.css.controller.manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import my.edu.utem.ftmk.css.model.Grouping;

/**
 * This class is used to manage the grouping data on the Grouping table.
 * 
 * @author Satrya Fajri Pratama
 * @version 1.0
 */
class GroupingManager extends AbstractTablemanager
{
	GroupingManager(Facade facade)
	{
		super(facade);
	}
	
	private void setGrouping(PreparedStatement ps, Grouping grouping, boolean add) throws SQLException
	{
		ps.setInt(1, grouping.getYearOfStudy());
		ps.setString(2, grouping.getCourse());
		ps.setInt(3, grouping.getSectionNo());
		ps.setInt(4, grouping.getGroupNo());
		
		if(!add)
			ps.setInt(5, grouping.getGroupingID());
	}
	
	int addGrouping(Grouping grouping) throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("INSERT INTO Grouping (YearOfStudy, Course, SectionNo, GroupNo)VALUES (?, ?, ?, ?)");
		
		setGrouping(ps,grouping, true);
		
		int status = ps.executeUpdate();
				
				if(status != 0)
				{
					ResultSet rs = ps.getGeneratedKeys();
					if (rs.next())
						grouping.setGroupingID(rs.getInt(1));
				}
		// Return the status
		return status;
	}
	
	int updateGrouping(Grouping grouping) throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("UPDATE Grouping SET YearOfStudy = ?, Course = ?, SectionNo = ?, GroupNo = ? WHERE GroupingID = ?");
		
		setGrouping(ps,grouping, false);
		
		
		// Return the status
		return ps.executeUpdate();
	}
	
	private Vector<Grouping> getGroupings(PreparedStatement ps) throws SQLException
	{
		// Send the statement and retrieve the results
		ResultSet rs = ps.executeQuery();
		
		// Create a list to store the results
		Vector<Grouping> groupings = new Vector<>();

		// Read the results
		while (rs.next())
		{
			Grouping grouping = new Grouping();
			
			grouping.setGroupingID(rs.getInt(1));
			grouping.setYearOfStudy(rs.getInt(2));
			grouping.setCourse(rs.getString(3));
			grouping.setSectionNo(rs.getInt(4));
			grouping.setGroupNo(rs.getInt(5));

			groupings.add(grouping);
		}
		
		// Return the status
		return groupings;
	}
	
	Vector<Grouping> getGroupings() throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("SELECT * FROM Grouping ORDER BY GroupingID, Course, SectionNo, GroupNo");
		
		return getGroupings(ps);
	}
	
	Vector<Grouping> getGroupings(String keyword, int type) throws SQLException
	{
		// Create statement object to be sent to the database
		PreparedStatement ps = facade.prepareStatement("SELECT * FROM Grouping WHERE LOWER(" + (type == 0 ? "GroupingID" : "Name") + ") LIKE ? ORDER BY GroupingID");
		
		ps.setString(1, "%" + keyword.toLowerCase() + "%");
		
		return getGroupings(ps);
	}
}