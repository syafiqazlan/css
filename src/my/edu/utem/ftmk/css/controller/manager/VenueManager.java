package my.edu.utem.ftmk.css.controller.manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import my.edu.utem.ftmk.css.model.Venue;

public class VenueManager extends AbstractTablemanager
{	
	public VenueManager(Facade facade)
	{
		super(facade);
	}
	
	public int addVenue(Venue venue) throws ClassNotFoundException, SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("INSERT INTO Venue Values (?)");
		
		ps.setString(1, venue.getVenueName());
		
		//return status
		return ps.executeUpdate();
	}

	public int updateVenue(Venue venue) throws ClassNotFoundException, SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("UPDATE Venue set VenueName = ?");
		
		ps.setString(1, venue.getVenueName());

		//return status
		return ps.executeUpdate();
	}
	
	public Vector<Venue> getVenues() throws ClassNotFoundException, SQLException
	{
		//prepared statement
		PreparedStatement ps = facade.prepareStatement("select * from Venue order by VenueID");
		
		//send statement and retrieve result
		ResultSet rs = ps.executeQuery();
		
		//create a list to store result
		Vector<Venue> venues = new Vector<>();
		
		//read result
		while (rs.next())
		{
			Venue venue = new Venue();
			
			venue.setVenueID(rs.getInt(1));
			venue.setVenueName(rs.getString(2));
			
			venues.add(venue);
		}
		
		//return status
		return venues;
	}

}
