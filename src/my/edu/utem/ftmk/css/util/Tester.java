/**
 * 
 */
package my.edu.utem.ftmk.css.util;

import java.sql.SQLException;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Staff;
import my.edu.utem.ftmk.css.model.Grouping;

/**
 * @author MP-
 *
 */
public class Tester 
{
	public static void main(String[] args) throws SQLException, ClassNotFoundException
	{
		Staff staff = new Staff();
		
		staff.setStaffID("baruas");
		staff.setPassword("123");
		staff.setName("baruuu");
		staff.setRole(1);
						
		Grouping Grouping = new Grouping();
//		Grouping.setGroupingID("BITP 3113");
//		Grouping.setName("OOP");
		
		Facade facade = new Facade();
		int status = facade.addStaff(staff);
		
		if(status != 0)
			System.out.println("Staff Added");
		else
			System.out.println("Faild to add staff");
		
		status = facade.addGrouping(Grouping);
		if(status != 0)
			System.out.println("Grouping Added");
		else
			System.out.println("Faild to add Grouping");
		
		facade.close();
	}
}
