package my.edu.utem.ftmk.css.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import my.edu.utem.ftmk.css.controller.manager.Facade;
import my.edu.utem.ftmk.css.model.Staff;
import my.edu.utem.ftmk.css.model.Grouping;

public class AddGroupingDialog extends JDialog implements ActionListener 
{
	private static final long serialVersionUID = 1L;

	private JTextField txtName = new JTextField(15);	
	private JTextField txtGroupingID = new JTextField(15); //size: 10 char
	private JButton btnSubmit = new JButton("Submit");
	private JButton btnReset = new JButton("Reset");
	
	public AddGroupingDialog(MainFrame frame)
	{
		super(frame, frame.getTitle() + " : Add Grouping", true);
		
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints label = new GridBagConstraints();
		GridBagConstraints input = new GridBagConstraints();
		
		label.fill = GridBagConstraints.BOTH;
		input.fill = GridBagConstraints.BOTH;
		input.gridwidth = GridBagConstraints.REMAINDER;
		
		label.insets = input.insets = new Insets(5,0,5,0); //padding
		
		JPanel pnlCenter = new JPanel(layout);
		JPanel pnlSouth = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		pnlCenter.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
		pnlSouth.setBorder(BorderFactory.createEmptyBorder(2, 0, 10, 10));
	
		JLabel lblID = new JLabel("ID: ");
		JLabel lblName = new JLabel("Name: ");
	
		layout.setConstraints(lblID, label);
		layout.setConstraints(lblName, label);
		
		layout.setConstraints(txtGroupingID, input);
		layout.setConstraints(txtName, input);

		pnlCenter.add(lblID);
		pnlCenter.add(txtGroupingID);
		
		pnlCenter.add(lblName);
		pnlCenter.add(txtName);
		
		pnlSouth.add(btnSubmit);
		pnlSouth.add(btnReset);
		
		btnSubmit.addActionListener(this);
		btnReset.addActionListener(this);
		
		this.add(pnlCenter, BorderLayout.CENTER);
		this.add(pnlSouth, BorderLayout.SOUTH);
		
		this.getRootPane().setDefaultButton(btnSubmit);
		this.setResizable(false);
		this.pack(); //set frame size to fit components
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null); //appear at center of screen
		this.setVisible(true);
	}
	
	private String validate(String label, String input, int maxlength, boolean required) throws Exception
	{
		input = input.trim();
		int length = input.length();
		
		if(required && length == 0)
			throw new Exception(label + " is required");
		
		if(length > maxlength)
			throw new Exception(label + " must less than " + maxlength + " characters.");
		 
		return input;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if (event != null)
		{
			Object source = event.getSource();
			
			if (source == btnSubmit)
			{
				Vector<Exception> exceptions = new Vector<>();
				String GroupingID = null;
				String name = null;

				try
				{
					name = validate("Grouping Name", txtName.getText(), 50, true);
				}
				catch (Exception e)
				{
					exceptions.add(e);
				}

				try
				{
					GroupingID = validate("Grouping ID", txtGroupingID.getText(), 10, true);
				}
				catch (Exception e)
				{
					exceptions.add(e);
				}
				
				int size = exceptions.size();
				
				if(size == 0)
				{
					Grouping Grouping = new Grouping();
					
					Grouping.setName(name);
					Grouping.setGroupingID(GroupingID);
					
					try (Facade facade = new Facade()) //class msti aoutocloseable, facade.close will awlays be called
					{
						int status = facade.addGrouping(Grouping);
						
						if(status != 0)
						{
							JOptionPane.showMessageDialog(this, "Success add a new Grouping with Grouping id: " + Grouping.getGroupingID(), getTitle(), JOptionPane.INFORMATION_MESSAGE);				
							this.dispose();
						}
						
						else				
							JOptionPane.showMessageDialog(this, "Unable to add new Grouping", getTitle(), JOptionPane.WARNING_MESSAGE);
						
						reset();
					} 
					catch (SQLException e)
					{
						e.printStackTrace();
						JOptionPane.showMessageDialog(this, e, getTitle(), JOptionPane.ERROR_MESSAGE);				
					}	
				}
				
				else
				{
					String message = "";
					
//					for (Exception e : exceptions)
//						message += " -" + e.getMessage() + "\n";
					
					if (size == 1)
						message = exceptions.get(0).getMessage();
					else
					{
						message = "Please check the following fields:";
						for (int i = 0; i < size; i++)
						{
							Exception e = exceptions.get(i);
							message += "\n" + (i + 1) + ". " + e.getMessage();
						}
					}
					
					JOptionPane.showMessageDialog(this, message, getTitle(), JOptionPane.WARNING_MESSAGE);												
				}
			}
			else if (source == btnReset)
			{
				reset();
			}
		}
	}
	
	private void reset()
	{
		txtGroupingID.setText("");
		txtName.setText("");
	}
}
