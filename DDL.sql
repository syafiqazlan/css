CREATE TABLE Staff
(
	StaffID varchar(20)
	constraint Staff_PK primary key,
	Password varchar(20) 
	constraint Password_NN not null,
	Name varchar(100) 
	constraint Staff_Name_NN not null,
	Role int
	constraint Role_NN not null
	constraint Role_CK CHECK (ROLE IN(0,1))
);

create table subject
(
	SubjectID varchar(10)
	constraint Subject_PK primary key,
	name varchar(50)
	constraint Subject_Name_NN not null
	constraint Subject_Name_UQ UNIQUE
);

create table Grouping
(
	GroupingID int
	constraint Grouping_PK primary key generated always as identity,
	YearOfStudy int
	constraint YearOfStudy_NN not null
	constraint YearOfStudy_CK check (YearOfStudy > 0),
	Course varchar(10)
	constraint Course_NN not null
	constraint Course_CK check (Course in ('BITC', 'BITD', 'BITE', 'BITI', 'BITM', 'BITS', 'BITZ')),
	SectionNo int
	constraint SectionNo_NN not null
	constraint SectionNo_CK check (SectionNo > 0),
	GroupNo int
	constraint GroupNo_NN not null
	constraint GroupNo_CK check (GroupNo > 0),	
	constraint Grouping_UQ unique (YearOfStudy, Course, SectionNo, GroupNo)
);

create table Clazz
(
	ClazzID int
	constraint Clazz_PK primary key generated always as identity (start with 1001, increment by 2),
	Type int
	constraint Type_NN not null
	constraint Type_CK check (Type in (1,2)),
	StaffID varchar(20)
	constraint Clazz_Staff_FK references Staff
	constraint Clazz_Staff_NN not null,
	SubjectID varchar(10)
	constraint Clazz_Subject_FK references Subject
	constraint Clazz_Subject_NN not null,
	GroupingID int
	constraint Clazz_Grouping_FK references Grouping
	constraint Clazz_Grouping_NN not null,
	constraint Clazz_UQ unique (Type, StaffID, SubjectID, GroupingID)
);

create table Venue
(
	VenueID int
	constraint Venue_PK primary key generated always as identity,
	VenueName varchar(100)
	constraint VenueName_NN not null
	constraint Venue_Name_UQ UNIQUE
);

create table Schedule
(
	ScheduleID int
	constraint Schedule_PK primary key generated always as identity,
	Start TIMESTAMP
	constraint Start_NN not null,
	Duration int
	constraint Duration_NN not null
	constraint Duration_CK check (Duration > 0),
	ClazzID int
	constraint Schedule_Clazz_FK references Clazz
	constraint Schedule_Clazz_NN not null,
	VenueID int
	constraint Schedule_Venue_FK references Venue
	constraint Schedule_Venue_NN not null
);